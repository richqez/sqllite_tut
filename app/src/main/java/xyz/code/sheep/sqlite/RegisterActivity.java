package xyz.code.sheep.sqlite;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import xyz.code.sheep.sqlite.xyz.code.sheep.UserDao;
import xyz.code.sheep.sqlite.xyz.code.sheep.model.User;

public class RegisterActivity extends AppCompatActivity {




    private Button btn_register,btn_cancel ;
    private EditText txt_username,txt_password,txt_conpassword,txt_name,txt_email;
    private UserDao userDao;
    private Context mContext ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mContext=this;
        userDao = new UserDao(this);
        //
        txt_username = (EditText) findViewById(R.id.txt_register_username);
        txt_password = (EditText) findViewById(R.id.txt_register_password);
        txt_conpassword = (EditText) findViewById(R.id.txt_register_conpassword);
        txt_name = (EditText) findViewById(R.id.txt_register_name);
        txt_email = (EditText) findViewById(R.id.txt_register_email);
        btn_register = (Button) findViewById(R.id.btn_register_register);
        btn_cancel = (Button) findViewById(R.id.btn_register_cancel);



        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearErrorMessage();
                if (validate_form()){

                    User new_register = new User();

                    new_register.setUsername(txt_username.getText().toString());
                    new_register.setPassword(txt_password.getText().toString());
                    new_register.setName(txt_name.getText().toString());
                    new_register.setEmail(txt_email.getText().toString());
                    userDao.createUser(new_register);
                    Toast.makeText(mContext,"Register success",Toast.LENGTH_LONG).show();
                    finish();
                }
            }
        });

    }


    private boolean validate_form(){

        boolean result = false ;

        if (txt_username.getText().toString().isEmpty()){
            txt_username.setError("username is require");
        }else if (txt_password.getText().toString().isEmpty()){
            txt_password.setError("password is require");
        }else if (txt_conpassword.getText().toString().isEmpty()){
            txt_conpassword.setError("confrimpassword is require");
        }else if (txt_name.getText().toString().isEmpty()){
            txt_name.setError("name is require");
        }else if (txt_email.getText().toString().isEmpty()){
            txt_email.setError("email is require");
        }else if (!txt_password.getText().toString().equals(txt_conpassword.getText().toString())){
            txt_conpassword.setError("confrim password not match");
        }else{
            result = true;
        }

        return  result ;
    }

    private void clearErrorMessage(){
        txt_username.setError(null);
        txt_password.setError(null);
        txt_conpassword.setError(null);
        txt_name.setError(null);
        txt_email.setError(null);
    }





}
