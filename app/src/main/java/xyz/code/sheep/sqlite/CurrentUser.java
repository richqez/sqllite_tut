package xyz.code.sheep.sqlite;

import xyz.code.sheep.sqlite.xyz.code.sheep.model.User;

/**
 * Created by root on 23/2/2559.
 */
public class CurrentUser {

    private static CurrentUser instance = null;
    private static User user ;
    protected CurrentUser(){

    }
    public static CurrentUser getInstance(){
        if (instance == null){
            return  new CurrentUser();
        }
        return instance;
    }

    public User getUser(){
        return this.user;
    }

    public void setUser(User user){
        this.user = user;
    }


}
