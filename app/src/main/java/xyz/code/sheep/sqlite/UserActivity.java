package xyz.code.sheep.sqlite;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import xyz.code.sheep.sqlite.xyz.code.sheep.UserDao;
import xyz.code.sheep.sqlite.xyz.code.sheep.model.User;

public class UserActivity extends AppCompatActivity {

    User currentUser ;
    EditText txt_password,txt_name,txt_email;
    Button btn_update;
    UserDao userDao ;
    Context mContext;
    TextView username,password,name,email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        userDao = new UserDao(this);
        mContext = this;
        currentUser = CurrentUser.getInstance().getUser();

        username = (TextView) findViewById(R.id.textView2);
        password = (TextView) findViewById(R.id.textView3);
        name = (TextView) findViewById(R.id.textView4);
        email = (TextView) findViewById(R.id.textView5);

        txt_password = (EditText) findViewById(R.id.txt_user_password);
        txt_name = (EditText) findViewById(R.id.txt_user_name);
        txt_email = (EditText) findViewById(R.id.txt_user_email);
        btn_update = (Button) findViewById(R.id.btn_user_update);

        refeshText();




        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User update_user = new User();
                update_user.setId(currentUser.getId());
                if (txt_password.getText().toString().isEmpty()){
                    update_user.setPassword(currentUser.getPassword());
                }else{
                    update_user.setPassword(txt_password.getText().toString());
                }

                if (txt_name.getText().toString().isEmpty()){
                    update_user.setPassword(currentUser.getName());
                }else{
                    update_user.setName(txt_name.getText().toString());
                }

                if (txt_email.getText().toString().isEmpty()){
                    update_user.setEmail(currentUser.getEmail());
                }else{
                    update_user.setEmail(txt_email.getText().toString());
                }

                User updated_user = userDao.update(update_user);
                CurrentUser.getInstance().setUser(updated_user);
                currentUser = CurrentUser.getInstance().getUser();
                clearText();
                Toast.makeText(mContext,"Update user success",Toast.LENGTH_LONG).show();
                refeshText();
            }
        });

    }

    private void clearText(){
        txt_password.setText(null);
        txt_email.setText(null);
        txt_name.setText(null);
    }

    private void refeshText(){
        username.setText(currentUser.getUsername());
        password.setText(currentUser.getPassword());
        name.setText(currentUser.getName());
        email.setText(currentUser.getEmail());
    }


}