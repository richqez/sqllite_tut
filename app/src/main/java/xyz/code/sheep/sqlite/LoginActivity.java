package xyz.code.sheep.sqlite;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import xyz.code.sheep.sqlite.xyz.code.sheep.UserDao;
import xyz.code.sheep.sqlite.xyz.code.sheep.model.User;

import static xyz.code.sheep.sqlite.R.id.txt_login_username;

public class LoginActivity extends AppCompatActivity {


    private Button btn_login,btn_register;
    private EditText txt_username,txt_password ;
    private UserDao userDao ;
    private Context mContext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        userDao = new UserDao(this);
        mContext = this ;

        txt_username = (EditText) findViewById(txt_login_username);
        txt_password = (EditText) findViewById(R.id.txt_login_password);
        btn_login = (Button) findViewById(R.id.btn_login_login);
        btn_register = (Button) findViewById(R.id.btn_login_register);

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this,RegisterActivity.class));
            }
        });

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearErrorMessage();
                if (validate_form()){
                    User loginUser = new User();
                    loginUser.setUsername(txt_username.getText().toString());
                    loginUser.setPassword(txt_password.getText().toString());
                    User foundUser = userDao.findByUsername(loginUser);
                    if (foundUser!=null){
                        if (!foundUser.getPassword().equals(txt_password.getText().toString())){
                            Toast.makeText(mContext,"Login fail username and password not match",Toast.LENGTH_LONG).show();
                        }else{
                            CurrentUser.getInstance().setUser(foundUser);
                            Toast.makeText(mContext, "LoginSucees Welcone " + CurrentUser.getInstance().getUser().getName(), Toast.LENGTH_LONG).show();
                            startActivity(new Intent(LoginActivity.this,UserActivity.class));
                            finish();
                        }


                    }else{
                        Toast.makeText(mContext,"Login fail cannot find username",Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

    }

    private boolean validate_form(){
        boolean result = false ;
        if (txt_username.getText().toString().isEmpty()){
            txt_username.setError("username is require");
        }else if (txt_password.getText().toString().isEmpty()){
            txt_password.setError("password is require");
        }else{
            result =true;
        }
        return result;
    }

    private void clearErrorMessage(){
        txt_password.setError(null);
        txt_username.setError(null);
    }
}
