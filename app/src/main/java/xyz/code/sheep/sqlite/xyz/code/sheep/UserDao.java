package xyz.code.sheep.sqlite.xyz.code.sheep;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import xyz.code.sheep.sqlite.xyz.code.sheep.model.User;

/**
 * Created by root on 23/2/2559.
 */
public class UserDao extends SQLiteOpenHelper {


    public static final int DATABASE_VERSION = 2;
    public static final String DATABASE_NAME = "kukai.db";

    public static final String USER_TABLE_NAME = "users";
    public static final String USER_COLUMN_ID = "id";
    public static final String USER_COLUMN_USERNAME = "username";
    public static final String USER_COLUMN_PASSWORD = "password";
    public static final String USER_COLUMN_NAME = "name" ;
    public static final String USER_COLUMN_EMAIL = "email";

    public static final String SQL_COMMAND_CREATE_USERS_TABLE = "CREATE TABLE "+USER_TABLE_NAME
            +"(" +USER_COLUMN_ID+" INTEGER PRIMARY KEY," +USER_COLUMN_USERNAME+" TEXT,"+USER_COLUMN_PASSWORD+" TEXT,"
            +USER_COLUMN_NAME+" TEXT,"+USER_COLUMN_EMAIL+" TEXT"
            +")";

    public static final String SQL_COMMAND_DROP_USERS_TABLE = "DROP TABLE IF EXISTS "+USER_TABLE_NAME;


    public UserDao(Context context){
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_COMMAND_CREATE_USERS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_COMMAND_DROP_USERS_TABLE);
        onCreate(db);
    }


    public boolean createUser(User user){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(USER_COLUMN_USERNAME,user.getUsername());
        contentValues.put(USER_COLUMN_PASSWORD,user.getPassword());
        contentValues.put(USER_COLUMN_NAME,user.getName());
        contentValues.put(USER_COLUMN_EMAIL, user.getEmail());
        sqLiteDatabase.insert(USER_TABLE_NAME, null, contentValues);
        return true;
    }

    public User findByUsername(User user){
        String sql_command = "SELECT * FROM "+USER_TABLE_NAME+" WHERE "+USER_COLUMN_USERNAME+"=" +"'"+user.getUsername()+"'";
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(sql_command,null);
        cursor.moveToFirst();
        if (cursor.getCount() < 1){
            return null;
        }else
        {
            User loginUser = new User();
            loginUser.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(USER_COLUMN_ID))));
            loginUser.setUsername(cursor.getString(cursor.getColumnIndex(USER_COLUMN_USERNAME)));
            loginUser.setPassword(cursor.getString(cursor.getColumnIndex(USER_COLUMN_PASSWORD)));
            loginUser.setName(cursor.getString(cursor.getColumnIndex(USER_COLUMN_NAME)));
            loginUser.setEmail(cursor.getString(cursor.getColumnIndex(USER_COLUMN_EMAIL)));
            return loginUser;
        }

    }

    public User update(User user){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(USER_COLUMN_PASSWORD,user.getPassword());
        contentValues.put(USER_COLUMN_NAME, user.getName());
        contentValues.put(USER_COLUMN_EMAIL, user.getEmail());
        sqLiteDatabase.update(USER_TABLE_NAME, contentValues, USER_COLUMN_ID + " = ? ", new String[]{Integer.toString(user.getId())});
        sqLiteDatabase.close();
        sqLiteDatabase = null;
        sqLiteDatabase = this.getReadableDatabase();
        String sql_command = "SELECT * FROM "+USER_TABLE_NAME+" WHERE "+USER_COLUMN_ID+"="+"'"+user.getId()+"'";
        Cursor cursor = sqLiteDatabase.rawQuery(sql_command,null);
        cursor.moveToFirst();
        User updated_user = new User();
        updated_user.setUsername(cursor.getString(cursor.getColumnIndex(USER_COLUMN_USERNAME)));
        updated_user.setPassword(cursor.getString(cursor.getColumnIndex(USER_COLUMN_PASSWORD)));
        updated_user.setName(cursor.getString(cursor.getColumnIndex(USER_COLUMN_NAME)));
        updated_user.setEmail(cursor.getString(cursor.getColumnIndex(USER_COLUMN_EMAIL)));
        return  updated_user;
    }



}
